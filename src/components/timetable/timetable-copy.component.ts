import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AppEventService } from '@universis/common';
import { EventsService } from '../../events.service';
import { Subscription, combineLatest } from 'rxjs';

@Component({
  selector: 'app-timetable-copy',
  template: ''
})
export class TimetableCopyComponent implements OnInit {

  private activatedRouteSubscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _appEvent: AppEventService,
    private _eventsService: EventsService) { }

  ngOnInit() {
    this.activatedRouteSubscription = combineLatest(
      this.activatedRoute.params,
      this.activatedRoute.data).subscribe(([params, routeData]) => {
        const { data, ...rest } = routeData;
        this.openModal({ ...params, ...data, ...rest });
      });
  }

  openModal(data: any) {
    const { timetable, model, action, continueLink = '../' } = data;
    this._eventsService.newEvent({
      formSrc: `${model}/${action}`,
      continueNavigation: {
        continueLink: continueLink,
        navigationExtras: {
          relativeTo: this.activatedRoute
        }
      },
      execute: (submissionData) => {
        return this._context.model(`${model}/${timetable}/copy`).save(submissionData);
      },
      afterExecute: () => this._appEvent.change.next({ model: 'CopyTimetableEventActions' })
    });
  }

  ngOnDestroy(): void {
    if (this.activatedRouteSubscription) {
      this.activatedRouteSubscription.unsubscribe();
    }
  }
}
