import { AdvancedColumnFormatter } from '@universis/ngx-tables';
import {ConfigurationService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

export class StatusFormatter extends AdvancedColumnFormatter {
    eventCompleted: any;
    eventOpen: any;
    render(data, type, row, meta) {
      const translateService: TranslateService = this.injector.get(TranslateService);

      if (data) {
        if (typeof data === 'object') {
          const { name, alternateName } = data;
          data = name || alternateName || data;
        }

        if (typeof data === 'string') {
          const translation = translateService.instant(`Events.Statuses.${data}`);
          let color: string;
          switch (data) {
            case 'EventOpened':
              color = 'text-info';
              break;
            case 'EventCompleted':
              color = 'text-danger';
              break;
            default:
              color = 'text-indigo';
          }
          return `<span class="${color}">${translation}</span>`;
        }
      }
      return data;
    }
    constructor( private configurationService: ConfigurationService ) {
      super();
    }
  }
